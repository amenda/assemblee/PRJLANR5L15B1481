# TITRE Ier

# GARANTIR LES SAVOIRS FONDAMENTAUX POUR TOUS

## Chapitre Ier

## L’engagement de la communauté éducative

### Article 1er

Après l’article L. 111‑3 du code de l’éducation, il est inséré un article L. 111‑3‑1 ainsi rédigé :

« Art. L. 111‑3‑1. – Par leur engagement et leur exemplarité, les personnels de la communauté éducative contribuent à l’établissement du lien de confiance qui doit unir les élèves et leur famille au service public de l’éducation. Ce lien implique également le respect des élèves et de leur famille à l’égard de l’institution scolaire et de l’ensemble de ses personnels. »

## Chapitre II

## L’extension de l’instruction obligatoire aux plus jeunes

### Article 2

Le premier alinéa de l’article L. 131‑1 du code de l’éducation est remplacé par les dispositions suivantes :

« L’instruction est obligatoire pour chaque enfant dès l’âge de trois ans et jusqu’à l’âge de seize ans. »

### Article 3

I. ‑ Le code de l’éducation est ainsi modifié :

1° Les deux premiers alinéas de l’article L. 113‑1 sont supprimés ;

2° Au troisième alinéa de l’article L. 131‑5, le mot : « six » est remplacé par le mot : « trois » ;

3° L’article L. 132‑1 est ainsi rédigé :

« Art. L. 132‑1. ‑ L’enseignement public dispensé dans les écoles maternelles et élémentaires est gratuit. » ;

4° Après l’article L. 212‑2, il est inséré un article L. 212‑2‑1 ainsi rédigé :

« Art. L. 212‑2. ‑ L’établissement des écoles maternelles publiques intervient dans les conditions prévues à l’article L. 212‑2. » ;

5° Au premier alinéa de l’article L. 212‑5, le mot : « élémentaires » est supprimé ;

6° À l’article L. 312‑9‑2, les mots : « dès le début de sa scolarité obligatoire » sont remplacés par les mots : « dès la première année de l’école élémentaire » ;

7° L’article L. 442‑3 est ainsi modifié :

a) Les mots : « d’écoles élémentaires privées qui ne sont pas liées » sont remplacés par les mots : « des établissements d’enseignement privés qui ne sont pas liés » ;

b) Le mot : « livres » est remplacé par les mots : « supports pédagogiques » ;

c) Les mots : « les articles L. 131‑1‑1 et L. 131‑10 » sont remplacés par les mots : « l’article L. 131‑1‑1 et de permettre aux élèves concernés l’acquisition progressive du socle commun défini à l’article L. 122‑1‑1. » ;

8° L’article L. 442‑5‑1 est ainsi modifié :

a) Au premier alinéa, le mot : « élémentaire » est supprimé ;

b) Au dernier alinéa, après les mots : « coût moyen des classes », sont insérés les mots : « maternelles et » ;

9° À l’article L. 442‑5‑2, après les mots : « dépenses de fonctionnement des classes », sont insérés les mots : « maternelles et » et les mots : « établissements privés du premier degré » sont remplacés par les mots : « établissements d’enseignement privés » ;

10° Au cinquième alinéa de l’article L. 452‑2, les mots : « l’enseignement élémentaire, secondaire ou supérieur » sont remplacés par les mots : « l’enseignement dans les classes maternelles et élémentaires, dans le second degré et dans le supérieur ».

II. ‑ À l’article 58 de la loi n° 2017‑256 du 28 février 2017 de programmation relative à l’égalité réelle outre‑mer et portant autres dispositions en matière sociale et économique, les mots : « entre trois ans et dix‑huit ans » sont remplacés par les mots : « entre seize ans et dix‑huit ans ».

### Article 4

L’État attribue à chaque commune les ressources correspondant à l’augmentation des dépenses obligatoires qu’elle a consenties en application des dispositions des articles L. 212‑4, L. 212‑5 et L. 442‑5 du code de l’éducation au titre de l’année scolaire 2019‑2020 par rapport à l’année scolaire précédente dans la limite de la part d’augmentation résultant directement de l’abaissement à trois ans de l’âge de l’instruction obligatoire.

Un décret en Conseil d’État fixe les modalités d’application du présent article.

## Chapitre III

## Le renforcement du contrôle de l’instruction dispensée dans la famille

### Article 5

L’article L. 131‑10 du code de l’éducation est ainsi modifié :

1° Au premier alinéa, après le mot : « responsables », sont insérés les mots : « de l’enfant » ;

2° Les troisième et quatrième alinéas sont rédigés de la manière suivante :

« L’autorité de l’État compétente en matière d’éducation doit au moins une fois par an, à partir du troisième mois suivant la déclaration d’instruction par les personnes responsables de l’enfant prévue au premier alinéa de l’article L. 131‑5, faire vérifier, d’une part, que l’enseignement assuré est conforme au droit de l’enfant à l’instruction tel que défini à l’article L. 131‑1‑1 et, d’autre part, que l’instruction dispensée au même domicile l’est pour les enfants d’une seule famille. Ce contrôle permet notamment de s’assurer de la maîtrise progressive par l’enfant de chacun des domaines du socle commun de connaissances, de compétences et de culture défini à l’article L. 122‑1‑1 au regard des objectifs de connaissances et de compétences attendues à la fin de chaque cycle d’enseignement de la scolarité obligatoire.

« Le contrôle est prescrit par l’autorité de l’État compétente en matière d’éducation selon des modalités qu’elle détermine. Il est organisé en principe au domicile où l’enfant est instruit. Les personnes responsables de l’enfant sont informées, à la suite de la déclaration annuelle qu’elles sont tenues d’effectuer en application du premier alinéa de l’article L. 131‑5, de l’objet et des modalités des contrôles qui seront conduits en application du présent article. » ;

3° Au cinquième alinéa, les mots : « la famille » sont remplacés par les mots : « les personnes responsables de l’enfant » ;

4° Les dispositions du sixième alinéa sont abrogées ;

5° Les septième et huitième alinéas sont remplacés par trois alinéas ainsi rédigés :

« Les résultats du contrôle sont notifiés aux personnes responsables de l’enfant. Lorsque ces résultats sont jugés insuffisants, les personnes responsables de l’enfant sont informées du délai au terme duquel un second contrôle est prévu. Elles sont également avisées des sanctions dont elles peuvent faire l’objet, au terme de la procédure, en application de l’article 227‑17‑1 du code pénal.

« Si les résultats du second contrôle sont jugés insuffisants, l’autorité de l’État compétente en matière d’éducation met en demeure les personnes responsables de l’enfant de l’inscrire, dans les quinze jours suivant la notification de cette mise en demeure et au moins jusqu’à la fin de l’année scolaire en cours, dans un établissement d’enseignement scolaire public ou privé et de faire aussitôt connaître au maire, qui en informe l’autorité de l’État compétente en matière d’éducation, l’école ou l’établissement qu’elles auront choisi.

« Lorsque les personnes responsables de l’enfant ont refusé, sans motif légitime, de soumettre leur enfant au contrôle annuel prévu au troisième alinéa, elles sont informées qu’en cas de second refus, sans motif légitime, l’autorité de l’État compétente en matière d’éducation est en droit de les mettre en demeure d’inscrire leur enfant, dans un établissement d’enseignement scolaire public ou privé dans les conditions et selon les modalités prévues au précédent alinéa. Elles sont également avisées des sanctions dont elles peuvent faire l’objet, au terme de la procédure, en application de l’article 227‑17‑1 du code pénal. » ;

6° L’article L. 131‑10 est complété par un alinéa ainsi rédigé :

« Un décret en Conseil d’État fixe les modalités d’application du présent article. »

# TITRE II

# INNOVER POUR S’ADAPTER AUX BESOINS DES TERRITOIRES

## Chapitre Ier

## L’enrichissement de l’offre de formation et l’adaptation des structures administratives aux réalités locales

### Article 6

I. ‑ La section 3 bis du chapitre Ier du titre II du livre IV du code de l’éducation est remplacée par les dispositions suivantes :

(2) « Section 3 bis

(3) « Les établissements publics locaux d’enseignement international

« Art. L. 421‑19‑1. ‑ Les établissements publics locaux d’enseignement international sont constitués de classes des premier et second degrés et dispensent tout au long de la scolarité des enseignements en langue française et en langue vivante étrangère. Ils préparent soit à l’option internationale du diplôme national du brevet et à l’option internationale du baccalauréat, soit au baccalauréat européen, délivré dans les conditions prévues par l’accord relatif à la modification de l’annexe au statut des écoles européennes et portant règlement du baccalauréat européen, signé à Luxembourg le 11 avril 1984. Les établissements publics locaux d’enseignement international préparant à l’option internationale du baccalauréat peuvent également préparer, au sein d’une section binationale, à la délivrance simultanée du baccalauréat général et du diplôme ou de la certification permettant l’accès à l’enseignement supérieur dans un État étranger en application d’accords passés avec lui.

« Ces établissements sont créés par arrêté du représentant de l’État dans le département sur proposition conjointe de la région, du ou des départements, de la ou des communes et du ou des établissements publics de coopération intercommunale compétents en matière de fonctionnement des écoles, après conclusion d’une convention entre ces collectivités et établissements publics de coopération intercommunale.

« Sous réserve des dispositions prévues par le présent chapitre, cet établissement est régi par les dispositions du titre préliminaire et du titre II du présent livre.

« Art. L. 421‑19‑2. – La convention mentionnée à l’article L. 421‑19‑1 fixe la durée pour laquelle elle est conclue et les conditions dans lesquelles, lorsqu’elle prend fin, les biens de l’établissement sont répartis entre les collectivités et les établissements publics de coopération intercommunale signataires. Elle détermine également le délai minimal qui ne peut être inférieur à une année scolaire au terme duquel peut prendre effet la décision de l’une des parties de se retirer de la convention.

« La convention fixe la répartition entre les parties des charges leur incombant en vertu des dispositions des chapitres II, III et IV du titre Ier du livre II au titre de la gestion des écoles, des collèges et des lycées. Elle définit notamment la répartition entre elles des charges liées à la construction, la reconstruction, l’extension, les grosses réparations, l’équipement et le fonctionnement de l’ensemble de l’établissement et des dépenses de personnels autres que ceux mentionnés à l’article L. 211‑8 qui exercent leurs missions dans l’établissement.

« La convention détermine la collectivité de rattachement de l’établissement et le siège de celui‑ci. La collectivité de rattachement assure les grosses réparations, l’équipement et le fonctionnement de l’ensemble de l’établissement ainsi que le recrutement et la gestion des personnels autres que ceux mentionnés à l’article L. 211‑8 qui exercent leurs missions dans l’établissement.

« En l’absence d’accord entre les signataires sur le contenu de la convention, soit lors de son renouvellement, soit à l’occasion d’une demande de l’un d’entre eux tendant à sa modification, le représentant de l’État fixe la répartition des charges entre les signataires en prenant en compte les effectifs scolarisés dans les classes maternelles, élémentaires, de collège et de lycée au sein de l’établissement public local d’enseignement international et désigne la collectivité de rattachement qui assure, jusqu’à l’intervention d’une nouvelle convention, les missions énoncées au troisième alinéa.

« Art. L. 421‑19‑3. – L’établissement public local d’enseignement international est dirigé par un chef d’établissement qui exerce les compétences attribuées au directeur d’école par l’article L. 411‑1 et les compétences attribuées au chef d’établissement par l’article L. 421‑3.

« Art. L. 421‑19‑4. – L’établissement public local d’enseignement international est administré par un conseil d’administration comprenant, outre le chef d’établissement et deux à quatre représentants de l’administration de l’établissement qu’il désigne, de vingt‑quatre à trente membres, dont :

« 1° Un tiers de représentants des collectivités territoriales et des établissements publics de coopération intercommunale parties à la convention mentionnée à l’article L. 421‑19‑1, et une ou plusieurs personnalités qualifiées ;

« 2° Un tiers de représentants élus du personnel de l’établissement ;

« 3° Un tiers de représentants élus des parents d’élèves et élèves.

« La convention mentionnée à l’article L. 421‑19‑1 fixe le nombre de membres du conseil d’administration qui comprend au moins un représentant par collectivité territoriale ou établissement public de coopération intercommunale partie à la convention. Lorsque le nombre de sièges réservés aux représentants de ces collectivités ou établissements publics en application du 1° de cet article n’est pas suffisant pour permettre la désignation d’un représentant pour chacun d’entre eux, la convention précise les modalités de leur représentation au conseil d’administration. Dans ce cas, la région, le département, la commune siège de l’établissement et, si elle est différente, la collectivité de rattachement, disposent chacun d’au moins un représentant.

« Lorsqu’une des parties à la convention dispose de plus d’un siège au conseil d’administration, l’un au moins de ses représentants est membre de son assemblée délibérante.

« Art. L. 421‑19‑5. – Le conseil d’administration de l’établissement public local d’enseignement international exerce les compétences du conseil d’administration mentionné à l’article L. 421‑4 ainsi que celles du conseil d’école mentionné à l’article L. 411‑1.

« Art. L. 421‑19‑6. – Outre les membres mentionnés à l’article L. 421‑5, le conseil pédagogique comprend au moins un enseignant de chaque niveau de classe du premier degré.

« Le conseil pédagogique peut être réuni en formation restreinte aux enseignants des niveaux, degrés ou cycles concernés par l’objet de la séance.

« Art. L. 421‑19‑7. – Les compétences des collectivités territoriales mentionnées aux articles L. 213‑2‑2 et L. 214‑6‑2 s’exercent dans les conditions prévues par ces articles après accord, le cas échéant, de la collectivité de rattachement désignée par la convention mentionnée à l’article L. 421‑19‑8.

« La convention mentionnée à l’article L. 421‑19‑1 peut prévoir que l’organe exécutif d’une collectivité territoriale ou d’un établissement public de coopération intercommunale signataire confie à l’organe exécutif de la collectivité de rattachement qu’elle a désigné le soin de décider, en son nom, d’autoriser l’utilisation des locaux et des équipements scolaires de l’établissement dans les conditions prévues à l’alinéa précédent.

« Art. L. 421‑19‑8. – Les élèves des classes maternelles et élémentaires de l’établissement public local d’enseignement international bénéficient du service d’accueil prévu par les articles L. 133‑1 à L. 133‑10.

« La convention mentionnée à l’article L. 421‑19‑1 peut prévoir que la commune confie l’organisation, pour son compte, de ce service d’accueil à la collectivité de rattachement de l’établissement public local d’enseignement international.

« Art. L. 421‑19‑9. – Le budget des établissements publics locaux d’enseignement international peut comprendre des concours de l’Union européenne ou d’autres organisations internationales, ainsi que des dons et legs dans les conditions prévues par le code général de la propriété des personnes publiques.

« Pour l’application des articles L. 421‑11 à L. 421‑16, la collectivité de rattachement de l’établissement public local d’enseignement international est celle ainsi désignée par la convention mentionnée à l’article L. 421‑19‑1, sans préjudice de la participation des autres collectivités et établissements publics de coopération intercommunale parties à cette convention aux dépenses d’équipement et de fonctionnement de cet établissement dans les conditions fixées au deuxième alinéa de ce dernier article.

« Art. L. 421‑19‑10. – L’admission des élèves à l’établissement public local d’enseignement international est soumise à la vérification de leur aptitude à suivre les enseignements dans la langue étrangère pour laquelle ils se portent candidats, dans des conditions fixées par décret.

« L’autorité compétente de l’État en matière d’éducation affecte dans l’établissement public local d’enseignement international les élèves qui ont satisfait à cette vérification d’aptitude.

« Art. L. 421‑19‑11. – Des enseignants peuvent être mis à disposition de l’établissement public local d’enseignement international par les Etats dont une des langues officielles est utilisée dans le cadre des enseignements dispensés dans l’établissement public local d’enseignement international.

« Art. L. 421‑19‑12. – Les établissements publics locaux d’enseignement international qui disposent de l’agrément délivré par le Conseil supérieur des écoles européennes dispensent des enseignements prenant en compte les principes de l’organisation pédagogique figurant dans la convention portant statut des écoles européennes faite à Luxembourg le 21 juin 1994.

« Par dérogation à l’article L. 122‑1‑1 et aux titres Ier, II et III du livre III, la scolarité dans les établissements mentionnés au premier alinéa est organisée en cycles pour lesquels ces écoles définissent les objectifs et les programmes de formation ainsi que les horaires de chaque année d’études et de chaque section conformément à ceux fixés par le Conseil supérieur des écoles européennes en application de la convention portant statut des écoles européennes.

« Le nombre des cycles et leur durée sont fixés par décret.

« Les établissements mentionnés au premier alinéa participent à l’organisation de l’examen du baccalauréat européen en accord avec le Conseil supérieur des écoles européennes conformément aux stipulations de l’accord relatif à la modification de l’annexe au statut des écoles européennes et portant règlement du baccalauréat européen, signé à Luxembourg le 11 avril 1984.

« Art. L. 421‑19‑13. – Les dispositions des titres Ier à V du livre V du code de l’éducation applicables aux élèves inscrits dans les écoles et à leur famille sont applicables aux élèves inscrits dans les classes du premier degré des établissements publics locaux d’enseignement international et à leur famille.

« Les dispositions des titres Ier à V du livre V du code de l’éducation applicables aux élèves inscrits dans les collèges et à leur famille sont applicables aux élèves des classes des niveaux correspondant à ceux des collèges des établissements publics locaux d’enseignement international et à leur famille.

« Les dispositions des titres Ier à V du livre V du code de l’éducation applicables aux élèves inscrits dans les lycées et à leur famille sont applicables aux élèves des classes des niveaux correspondant à ceux des lycées des établissements publics locaux d’enseignement international et à leur famille.

« Art. L. 421‑19‑14. – Les commissions consultatives exclusivement compétentes en matière de vie des élèves au sein des établissements publics locaux d’enseignement international sont composées de manière à ce qu’un nombre égal de représentants des élèves de chaque sexe soit élu.

« Art. L. 421‑19‑15. – Une association sportive est créée dans tous les établissements publics locaux d’enseignement international. Les dispositions des articles L. 552‑2 à L. 552‑4 lui sont applicables.

« Art. L. 421‑19‑16. – Un décret en Conseil d’État fixe les conditions d’application du présent chapitre. »

II. – Au deuxième alinéa de l’article L. 3214‑2 du code général des collectivités territoriales, les mots : « Du proviseur ou du principal » sont remplacés par les mots : « Du chef d’établissement » et les mots : « les lycées ou les collèges » sont remplacés par les mots : « les établissements publics d’enseignement ».

III. – Les dispositions de la section 3 bis du titre II du livre IV du code de l’éducation sont abrogées.

IV. – Dans leur version en vigueur à la date de la promulgation de la présente loi, l’arrêté du préfet du département du Bas‑Rhin pris en application de l’article L. 421‑19‑1 du code de l’éducation dans sa version en vigueur avant la promulgation de la présente loi et la convention conclue sur le fondement des mêmes dispositions sont réputés pris sur le fondement des dispositions de la section 3 bis du chapitre Ier du titre II du livre IV du code de l’éducation dans leur version issue de la présente loi.

### Article 7

I. – Le code de l’éducation est ainsi modifié :

1° À l’article L. 262‑1, les mots : « et le premier alinéa de l’article L. 222‑1 » sont supprimés ;

2° À l’article L. 262‑5, le mot : « vice‑recteur » est remplacé par les mots : « recteur d’académie » ;

3° Les articles L. 162‑2‑1, L. 372‑1‑1, L. 492‑1‑1, L. 682‑1, L. 682‑2 et le premier alinéa de l’article L. 772‑1 sont abrogés.

II. ‑ L’article L. 361‑1 du code de la recherche est abrogé.

III. – Le 19° de l’article L. 1521‑2‑2 du code du travail est supprimé.

## Chapitre II

## Le recours à l’expérimentation

### Article 8

I. – Le code de l’éducation est ainsi modifié :

1° Dans l’intitulé du chapitre IV du titre Ier du livre III de la deuxième partie, après le mot : « recherche », sont insérés les mots : « , l’expérimentation » ;

2° L’article L. 314‑1 est ainsi rédigé :

« Art. L. 314‑1. – Des travaux de recherche en matière pédagogique peuvent se dérouler dans des écoles et des établissements publics ou privés sous contrat.

« Lorsque ces travaux de recherche impliquent des expérimentations conduisant à déroger aux dispositions du présent code, ces dérogations sont mises en œuvre dans les conditions prévues par l’article L. 314‑2. » ;

3° L’article L. 314‑2 est ainsi rédigé :

« Art. L. 314‑2. –  Sous réserve de l’autorisation préalable des autorités académiques, le projet d’école ou d’établissement mentionné à l’article L. 401‑1 peut prévoir la réalisation, dans des conditions définies par décret, d’expérimentations pédagogiques portant sur tout ou partie de l’école ou de l’établissement, d’une durée limitée à cinq ans. Ces expérimentations peuvent concerner l’organisation pédagogique de la classe, de l’école ou de l’établissement, la coopération avec les partenaires du système éducatif, les échanges avec des établissements étrangers d’enseignement scolaire, l’utilisation des outils et ressources numériques, la répartition des heures d’enseignement sur l’ensemble de l’année scolaire dans le respect des obligations réglementaires de service des enseignants et les procédures d’orientation des élèves.

« Dans ce cas, l’accès aisé à une école ou à un établissement ne pratiquant pas une telle expérimentation doit être garanti aux élèves dont les familles le désirent.

« Les modalités d’évaluation de ces expérimentations et de leur éventuelle reconduction sont fixées par décret. » ;

4° Les deux derniers alinéas de l’article L. 401‑1 sont supprimés ;

II. ‑ Lorsque des expérimentations ont été autorisées sur le fondement de l’article L. 401 ‑ 1 dans sa rédaction antérieure à la présente loi, elles se poursuivent jusqu’au terme de la période pour laquelle elles ont été autorisées.

## Chapitre III

## L’évaluation au service de la communauté éducative

### Article 9

I. – Le chapitre Ier bis du titre IV du livre II du code de l’éducation est remplacé par les dispositions suivantes :

(2) « Chapitre Ier bis

(3) « Le conseil d’évaluation de l’école

« Art. L. 241‑12. – Le conseil d’évaluation de l’école, placé auprès du ministre chargé de l’éducation nationale, est chargé d’évaluer en toute indépendance l’organisation et les résultats de l’enseignement scolaire. À ce titre :

« 1° Il veille à la cohérence des évaluations conduites par le ministère chargé de l’éducation nationale portant sur les acquis des élèves, les dispositifs éducatifs et les établissements d’enseignement scolaire. À ce titre, il établit une synthèse des différents travaux d’évaluation sur le système éducatif et a pour mission d’enrichir le débat public sur l’éducation ;

« 2° Il définit le cadre méthodologique et les outils des évaluations des établissements conduites par le ministère chargé de l’éducation nationale et analyse les résultats de ces évaluations ; il s’assure de la fréquence régulière de celles‑ci et définit les modalités de leur publicité ;

« 3° Il donne un avis sur les méthodologies, sur les outils et sur les résultats des évaluations du système éducatif organisées au niveau national par les services du ministre chargé de l’éducation nationale ou dans le cadre de programmes de coopération européens ou internationaux.

« Il formule toute recommandation utile au regard des résultats des évaluations mentionnées au présent article.

« Il établit une proposition de programme de travail annuel, qu’il soumet pour avis au ministre chargé de l’éducation nationale dans des conditions prévues par arrêté du ministre chargé de l’éducation nationale.

« Art. L. 241‑13. – Le conseil d’évaluation de l’école est composé de douze membres de nationalité française et étrangère. Il comprend, à parité de femmes et d’hommes pour chacun des collèges mentionnés aux 1° et 2° :

« 1° Quatre personnalités choisies par le ministre chargé de l’éducation nationale pour leur compétence en matière d’évaluation ou dans le domaine éducatif ;

« 2° Deux députés et deux sénateurs ;

« 3° Quatre représentants du ministre chargé de l’éducation nationale.

« Les membres mentionnés au 2° sont désignés pour la durée de leur mandat parlementaire. La durée et les modalités de renouvellement du mandat des membres mentionnés au 1° sont fixées par décret.

« Art. L. 241‑14. – Le rapport, les avis et les recommandations du conseil d’évaluation de l’école sont rendus publics. »

II. – Au deuxième alinéa de l’article L. 231‑14, les mots : « conseil national d’évaluation du système scolaire » sont remplacés par les mots : « conseil d’évaluation de l’école ».

# TITRE III

# AMÉLIORER LA GESTION DES RESSOURCES HUMAINES

## Chapitre IER

## Les instituts nationaux supérieurs du professorat et de l’éducation

### Article 10

L’article L. 625‑1 du code de l’éducation est ainsi modifié :

1° Au premier alinéa, les mots : « écoles supérieures du professorat et de l’éducation » sont remplacés par les mots : « instituts nationaux supérieurs du professorat et de l’éducation » et le mot : « elles » est remplacé par le mot : « ils » ;

2° La première phrase du second alinéa de l’article L. 625‑1 du code de l’éducation est complétée par les mots : « , ainsi que le référentiel de formation correspondant. ».

### Article 11

I. – L’intitulé du titre II du livre VII du code de l’éducation est remplacé par un intitulé ainsi rédigé :

(2) « Titre II : Instituts nationaux supérieurs du professorat et de l’éducation ».

II. – Dans l’intitulé du chapitre II du titre II du livre VII du code de l’éducation, les mots : « des maîtres et les écoles » sont remplacés par les mots : « des maîtres, les écoles » et après les mots : « et de l’éducation », sont ajoutés les mots : « et les instituts nationaux supérieurs du professorat et de l’éducation ».

III. – Le second alinéa de l’article L. 722‑1 du code de l’éducation est complété par les mots : « et, à compter de la date d’entrée en vigueur de la loi n°        du          pour une école de la confiance, ces biens sont affectés aux instituts nationaux supérieurs du professorat et de l’éducation ».

IV. – Aux articles L. 625‑1, L. 683‑2‑1, L. 721‑1 à L. 721‑3, L. 713‑1, L. 718‑8, L. 722‑16, L. 722‑17, L. 773‑3‑1, L. 774‑3‑1, L. 912‑1‑2 et L. 932‑3 du code de l’éducation, ainsi qu’aux articles L. 3321‑1, L. 3664‑1, L. 4425‑29, L. 71‑113‑3, L. 72‑103‑2 du code général des collectivités territoriales, les mots : « écoles supérieures du professorat et de l’éducation » sont remplacés par les mots : « instituts nationaux supérieurs du professorat et de l’éducation ».

### Article 12

Le I de l’article L. 721‑3 du code de l’éducation est ainsi modifié :

1° Le cinquième alinéa est remplacé par les dispositions suivantes :

« Le directeur de l’institut est nommé par arrêté conjoint des ministres chargés de l’éducation nationale et de l’enseignement supérieur. » ;

2° Il est ajouté deux alinéas ainsi rédigés :

« Les candidats à l’emploi de directeur d’institut sont auditionnés par un comité coprésidé par le recteur territorialement compétent et le président ou le directeur de l’établissement de rattachement.

« Un décret précise la durée des fonctions de directeur de l’institut, les conditions à remplir pour pouvoir être candidat à cet emploi ainsi que les modalités de désignation et de fonctionnement du comité d’audition. »

## Chapitre II

## Les personnels au service de la mission éducative

### Article 13

L’article L. 911‑5 du code de l’éducation est ainsi modifié :

1° Le premier alinéa est remplacé par les dispositions suivantes :

« I. – Sont incapables de diriger un établissement d’enseignement du premier ou du second degré, ou tout établissement de formation accueillant un public d’âge scolaire, qu’il soit public ou privé, ou d’y être employés, à quelque titre que ce soit : » ;

2° Au deuxième alinéa, les mots : « subi une condamnation judiciaire » sont remplacés par les mots : « été définitivement condamnés par le juge pénal » ;

3° Au quatrième alinéa, les mots : « définitive d’enseigner » sont remplacés par les mots  « d’exercer, à titre définitif, une fonction d’enseignement ou une activité professionnelle ou bénévole impliquant un contact habituel avec des mineurs » ;

4° Le cinquième alinéa est remplacé par les dispositions suivantes :

« II. – Est incapable de diriger un établissement d’enseignement du premier ou du second degré, ou tout établissement de formation accueillant un public d’âge scolaire, qu’il soit public ou privé, ou d’y être employée, toute personne qui, ayant exercé dans un établissement d’enseignement ou de formation accueillant un public d’âge scolaire, a été révoquée ou licenciée en application d’une sanction disciplinaire prononcée en raison de faits contraires à la probité et aux mœurs. » ;

5° Le dernier alinéa est supprimé.

### Article 14

L’article L. 916‑1 du même code est ainsi modifié :

1° Après le premier alinéa, il est inséré un alinéa ainsi rédigé :

« Les assistants d’éducation qui sont inscrits dans une formation dispensée par un établissement d’enseignement supérieur délivrant un diplôme préparant au concours d’accès aux corps des personnels enseignants ou d’éducation peuvent se voir confier des fonctions pédagogiques, d’enseignement ou d’éducation. » ;

2° Les deuxième et troisième phrases du dernier alinéa sont remplacées par les dispositions suivantes :

« Ce décret précise les droits reconnus aux assistants d’éducation au titre des articles L. 970‑1 et suivants du code du travail, les modalités d’aménagement de leur temps de travail, en particulier pour ceux qui sont astreints à un service de nuit, ainsi que les conditions dans lesquelles les assistants d’éducation recrutés en application du deuxième alinéa du présent article peuvent exercer des fonctions pédagogiques, d’enseignement ou d’éducation. »

### Article 15

Au titre Ier du livre IX du code de l’éducation, est ajouté un chapitre VIII, ainsi rédigé :

(2) « Chapitre VIII

(3) « dispositions relatives à divers personnels intervenant en matière d’éducation

« Art. L. 918‑1. – Les statuts particuliers des corps de personnels d’éducation, de psychologues de l’éducation nationale, de personnels de direction des établissements d’enseignement et de personnels d’inspection relevant du ministère de l’éducation nationale peuvent déroger, après avis du Conseil supérieur de la fonction publique de l’État, aux dispositions de la loi n° 84‑16 du 11 janvier de 1984 portant dispositions statutaires relatives à la fonction publique de l’État pour répondre aux besoins propres de la gestion de ces corps. »

### Article 16

L’article L. 952‑6 du code de l’éducation est ainsi modifié :

1° Au deuxième alinéa :

a) Après la première phrase, est insérée une phrase ainsi rédigée :

« Toutefois, les statuts d’un établissement public d’enseignement supérieur peuvent prévoir que le président ou le directeur de l’établissement peut présider la formation restreinte aux enseignants‑chercheurs du conseil d’administration ou du conseil académique ou des organes en tenant lieu. Dans ce cas, le président ou le directeur ne peut participer à l’examen des questions individuelles que dans le respect des principes rappelés au présent alinéa. » ;

b) Dans la deuxième phrase, devenue troisième phrase, les mots : « Toutefois, » sont supprimés.

2° Au troisième alinéa, les mots : « avec l’avis du président ou du directeur de l’établissement. » sont supprimés.

# TITRE IV

# SIMPLIFIER LE SYSTÈME ÉDUCATIF

## Article 17

Le Gouvernement est autorisé à prendre par ordonnance, dans les conditions prévues à l’article 38 de la Constitution et dans un délai d’un an à compter de la publication de la présente loi, les mesures relevant du domaine de la loi rendues nécessaires par le nouveau découpage territorial des circonscriptions académiques et la réorganisation, sur le territoire national, des services déconcentrés relevant des ministères chargés de l’éducation nationale et de l’enseignement supérieur, dans le périmètre des circonscriptions administratives régionales de l’État.

Le projet de loi de ratification est déposé devant le Parlement dans un délai de quatre mois à compter de la publication de l’ordonnance.

## Article 18

Dans les conditions prévues à l’article 38 de la Constitution, le Gouvernement est autorisé à prendre par ordonnance, dans un délai de douze mois à compter de la publication de la présente loi, les mesures relevant du domaine de la loi permettant, d’une part, de simplifier l’organisation et le fonctionnement, sur l’ensemble du territoire national, des conseils de l’éducation nationale mentionnés aux chapitres IV et V du titre III du livre II de la première partie du code de l’éducation et, d’autre part, de redéfinir et d’adapter les attributions de ces conseils, afin de tenir compte notamment de l’évolution des compétences des collectivités territoriales.

Un projet de loi de ratification est déposé devant le Parlement dans un délai de quatre mois à compter de la publication de l’ordonnance.

## Article 19

Après le quatrième alinéa de l’article L. 531‑4, il est inséré un alinéa ainsi rédigé :

« Ces bourses sont à la charge de l’État. Elles sont servies, pour les élèves inscrits dans un établissement public, par l’établissement, après déduction éventuelle des frais de pension ou de demi‑pension et, pour les élèves inscrits dans un établissement d’enseignement privé, par les services académiques. »

## Article 20

Le II de l’article 23 de la loi n° 2017‑257 du 28 février 2017 relative au statut de Paris et à l’aménagement métropolitain est remplacé par un II ainsi rédigé :

« II. – Il est créé une caisse des écoles du premier secteur de Paris à compter de la date d’entrée en vigueur de l’article 21.

« Par délibérations concordantes des comités de gestion des caisses concernées ou au plus tard le 1er janvier 2021, cette caisse sera substituée de plein droit aux caisses des écoles des 1er, 2e, 3e et 4e arrondissements dans l’ensemble de leurs missions, droits et obligations, dans toutes les délibérations et tous les actes qui relevaient de leur compétence, toutes les procédures administratives et juridictionnelles en cours ainsi que tous les contrats en cours. Ces contrats seront exécutés dans les conditions antérieures jusqu’à leur échéance, sauf accord contraire des parties. Les cocontractants seront informés de la substitution de personne morale. Cette substitution n’entraînera aucun droit à résiliation ou à indemnisation pour le cocontractant. Le conseil d’administration de la caisse des écoles du premier secteur est compétent pour approuver les comptes des caisses des écoles des 1er, 2e, 3e et 4e arrondissements.

« Les transferts de biens des caisses des écoles des 1er, 2e, 3e et 4e arrondissements vers la caisse des écoles du premier secteur seront réalisés à titre gratuit à compter de la date mentionnée au deuxième alinéa. Les transferts de biens, droits et obligations ne donneront lieu ni au versement de la contribution prévue à l’article 879 du code général des impôts, ni à la perception d’impôts, de droits ou de taxes de quelque nature que ce soit.

« À titre transitoire, jusqu’à la date mentionnée au deuxième alinéa, les représentants de la commune dans ces caisses des écoles seront désignés par le maire du premier secteur d’arrondissements dans les conditions mentionnées à l’article L. 2511‑29 du code général des collectivités territoriales. »

## Article 21

I. – La deuxième phrase du deuxième alinéa de l’article L. 953‑2 du code de l’éducation est supprimée.

II. – À compter de la date d’entrée en vigueur du présent article, la liste d’aptitude établie au titre de l’année scolaire 2018‑2019 est caduque.

# TITRE V

# DISPOSITIONS DIVERSES

## Article 22

Le Gouvernement est autorisé à procéder par ordonnance, dans les conditions prévues à l’article 38 de la Constitution, à la révision et à l’actualisation des dispositions de nature législative particulières à l’outre‑mer en vigueur à la date de publication de l’ordonnance, au sein du code de l’éducation, en vue :

1° de remédier aux éventuelles erreurs ou insuffisances de codification, en incluant les dispositions de nature législative qui n’auraient pas été codifiées et en adaptant le plan et la rédaction des dispositions codifiées ;

2° d’abroger les dispositions obsolètes, inadaptées ou devenues sans objet ;

3° d’adapter, le cas échéant, ces dispositions à l’évolution des caractéristiques et contraintes particulières aux collectivités régies par l’article 73 de la Constitution ;

4° d’étendre, le cas échéant dans le respect des règles de partage des compétences prévues par la loi organique, l’application de ces dispositions, selon le cas, à Saint‑Pierre‑et‑Miquelon, à Saint‑Barthélemy, à Saint‑Martin, en Nouvelle‑Calédonie, en Polynésie française et dans les îles Wallis et Futuna, avec les adaptations nécessaires, et de procéder, si nécessaire, à l’adaptation des dispositions déjà applicables à ces collectivités ;

5° de mettre les autres codes et lois qui mentionnent ces dispositions en cohérence avec la nouvelle rédaction adoptée.

L’ordonnance mentionnée au premier alinéa est prise dans un délai de dix‑huit mois à compter de la promulgation de la présente loi. Un projet de loi de ratification est déposé devant le Parlement dans un délai de trois mois à compter de la publication de l’ordonnance.

## Article 23

I. – Le I de l’article 125 de la loi n° 2013‑660 du 22 juillet 2013 relative à l’enseignement supérieur et à la recherche est complété par un alinéa ainsi rédigé :

« L’article 39 de la présente loi est applicable dans les îles Wallis et Futuna, en Polynésie française et en Nouvelle‑Calédonie dans sa rédaction résultant de la loi n° 2018‑166 du 8 mars 2018 relative à l’orientation et à la réussite des étudiants. »

II. – À l’article L. 442‑20 du code de l’éducation, les mots : « le premier alinéa de l’article L. 113‑1, les articles » sont supprimés et, après la référence : « L. 313‑1 », sont insérées les références : « L. 314‑1 et L. 314‑2 ».

III. – Au premier alinéa de l’article L. 612‑3‑2, les mots : « délivré au nom de l’État dans les conditions prévues à l’article L. 335‑6 » sont remplacés par les mots : « mentionné au I de l’article L. 6113‑5 du code du travail ».

## Article 24

Les ordonnances suivantes sont ratifiées :

1° a) L’ordonnance n° 2014‑691 du 26 juin 2014 portant suppression des compétences contentieuses et disciplinaires du Conseil supérieur de l’éducation et des conseils académiques de l’éducation nationale ;

b) L’article 23 de la même ordonnance est ainsi modifié :

– le I est remplacé par les dispositions suivantes :

« I. – À l’article L. 261‑1 du même code, les références : “L. 231‑1 à L. 231‑17” sont remplacées par les références : “L. 231‑1 à L. 231‑5, L. 231‑14 à L. 231‑17”.

« I bis. – Aux articles L. 263‑1 et L. 264‑1 du même code, les références : "L. 231‑1 à L. 231‑13" sont remplacées par les références : "L. 231‑1 à L. 231‑5" » ;

– le premier alinéa du III est remplacé par les dispositions suivantes :

« III. – Après le premier alinéa de l’article L. 973‑1 du même code, il est inséré un alinéa ainsi rédigé : » ;

– le premier alinéa du IV est remplacé par les dispositions suivantes :

« IV.  – Après le premier alinéa de l’article L. 974‑1 du même code, il est inséré un alinéa ainsi rédigé : » ;

2° L’ordonnance n° 2014‑692 du 26 juin 2014 relative à l’application à Mayotte de la loi n° 2013‑595 du 8 juillet 2013 d’orientation et de programmation pour la refondation de l’école de la République ;

3° L’ordonnance n° 2014‑693 du 26 juin 2014 portant extension et adaptation dans les îles Wallis et Futuna, en Polynésie française et en Nouvelle‑Calédonie de la loi n° 2013‑595 du 8 juillet 2013 d’orientation et de programmation pour la refondation de l’école de la République ;

4° a) L’ordonnance n° 2014‑135 du 17 février 2014 modifiant la partie législative du code de la recherche ;

b) Au 4° de l’article L. 114‑3‑1 du code de la recherche, les mots : « III du titre Ier du livre IV » sont remplacés par les mots : « I du titre III du livre V » ;

5° a) L’ordonnance n° 2015‑24 du 14 janvier 2015 portant extension et adaptation dans les îles Wallis et Futuna, en Polynésie française et en Nouvelle‑Calédonie de la loi n° 2013‑660 du 22 juillet 2013 relative à l’enseignement supérieur et à la recherche ;

b) La deuxième phrase du dernier alinéa de l’article L. 773‑2 du code de l’éducation est remplacée par la phrase suivante :

« Toutefois, dans le conseil d’administration siègent trois représentants de la Polynésie française, les autres catégories de personnalités extérieures disposant d’au moins un représentant. » ;

6° L’ordonnance n° 2015‑25 du 14 janvier 2015 relative à l’application à Mayotte de la loi n° 2013‑660 du 22 juillet 2013 relative à l’enseignement supérieur et à la recherche et de l’article 23 de la loi n° 2014‑288 du 5 mars 2014 relative à la formation professionnelle, à l’emploi et à la démocratie sociale.

## Article 25

Les articles 1er à 6, 8 à 12, 14, 19 et 21 entrent en vigueur à la rentrée scolaire 2019.

L’article 7 entre en vigueur le 1er janvier 2020.